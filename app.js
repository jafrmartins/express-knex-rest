const express = require('express')
const bodyParser = require('body-parser')
const config = require('./expressfile')
const router = require('./middleware/router')
const { EventEmitter } = require("events")
const events = new EventEmitter()
require('./app-events')(events)

const app = express()
app.use(express.static(config.static))
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use((req, res, next) => {
  req.events = events
  next();
})
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', config.origin);
  res.setHeader('Access-Control-Allow-Methods', config.methods);
  res.setHeader('Access-Control-Allow-Headers', config.headers);
  next();
})

app.use((error, req, res, next) => {
  if(error) {
    res.status(500).json({ error })
  } else { next() }
})

app.use('/', router())

app.listen(config.port, () => {
  console.log(`Server running on port ${config.port}`)
})
