const knex = require('knex')
const default_connection = require(`${process.cwd()}/knexfile`)

function getDatabase(connection) {
  if (connection) {
    connection = require("../app-connections")[connection]
  } else {
    connection = default_connection
  }
  if (!connection) { throw new Error("Invalid connection")}
  const db = knex(connection)
  return db;
}

module.exports = {
  all ({ table, connection, limit }) {
    const db = getDatabase(connection)
    let query = db(table)
    if (typeof limit != "undefined") {
      query = query.limit(limit)
    }
    return new Promise((resolve) => {
      query.then((data) => {
        db.destroy().then(() => {
          resolve(data)
        })
      })
    })
  },
  get ({ table, q, connection, limit }) {
    const db = getDatabase(connection)
    let query = db(table).where(q)
    if (typeof limit != "undefined") {
      query = query.limit(limit)
    }
    return new Promise((resolve) => {
      query.then((data) => {
        db.destroy().then(() => {
          resolve(data)
        })
      })
    })
  },
  post ({ table, body, connection }) {
    const db = getDatabase(connection)
    return new Promise((resolve) => {
      db(table).insert(body).returning("id").into(table).then((data) => {
        db.destroy().then(() => {
          resolve(data)
        })
      })
    })
  },
  put ({ table, body, q, connection }) {
    const db = getDatabase(connection)
    return new Promise((resolve) => {
      db(table).where(q).update(body).then((data) => {
        db.destroy().then(() => {
          resolve(data)
        })
      })
    })
  },
  delete({ table, q, connection }) {
    const db = getDatabase(connection)
    return new Promise((resolve) => {
      db(table).where(q).del().then((data) => {
        db.destroy().then(() => {
          resolve(data)
        })
      })
    })
  }
}