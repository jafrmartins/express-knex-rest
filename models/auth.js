const { saltHashPassword, randomToken } = require('../lib/utils')
const knex = require('knex')

module.exports = {
  getUser(where, connection) {
    if(!connection) {
      connection = knex(require(`${process.cwd()}/knexfile`))
    }
    return connection('users')
      .select([
        'users.id', 
        'users.salt', 
        'users.password', 
        'users.email'
      ])
      .where(where)
      .then(([user]) => {
        return connection('grants')
        .join('user_grants', 'grants.id', 'user_grants.grant_id')
        .join('users', 'users.id', 'user_grants.user_id')
        .select([
          'grants.name'
        ])
        .where({ 'users.id': user.id })
        .then((grants) => {
          return new Promise((resolve, reject) => {
            if (user && grants) {
              user.grants = grants.map((grant) => { return grant.name })
              return resolve(user)
            } reject('Could not find a valid user');
          })
        })
      })
  },
  getUserByEmail ({ email }) {
    return this.getUser({ email })
  },
  registerUser({ email, password }) {
    const connection = knex(require(`${process.cwd()}/knexfile`))
    const { salt, hash } = saltHashPassword({ password })
    return connection('users')
      .insert({ email, password: hash, salt })
      .returning('id')
      .into('users')
      .then(([id]) => {
        return new Promise((resolve, reject) => {
          if (!id) { return reject("Error Inserting User"); }
          connection('user_grants')
          .insert({ user_id: id, grant_id: 2 })
          .then(() => { resolve(id) })
        })
      })
  },
  createUserToken(user_id, token=randomToken(48)) {
    const connection = knex(require(`${process.cwd()}/knexfile`))
    return connection('user_tokens')
      .insert({ user_id, token })
      .then(() => {
        return new Promise((resolve, reject) => {
          if (!token) { return reject("Error Creating Token") }
          resolve(token)
        })
      })
  },
  verifyUserToken(token) {
    const self = this;
    const connection = knex(require(`${process.cwd()}/knexfile`))
    return connection('user_tokens')
      .select([
        'user_tokens.user_id',
        'user_tokens.token'
      ])
      .where({ token })
      .then(([user_token]) => {
        if (!user_token) { throw new Error("Invalid Token") }
        return self.getUser({ id: user_token.user_id }, connection)
      })
  },
  deleteUserToken(token) {
    const connection = knex(require(`${process.cwd()}/knexfile`))
    return connection('user_tokens').where({ token }).del()
  }
}
