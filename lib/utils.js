const config = require("../expressfile")
const crypto = require("crypto");
const jwt = require("jsonwebtoken")

function saltHashPassword({
  password,
  salt = randomToken()
}) {
  const hash = crypto
    .createHmac('sha512', salt)
    .update(password)
  return {
    salt,
    hash: hash.digest('hex')
  }
};

function randomToken(length=4) {
  return crypto.randomBytes(length).toString('hex')
};

function createJwtToken(user) {

  let token = jwt.sign({
     data: {
       id: user.id,
       email: user.email,
       grants: user.grants
     }
    }, config.jwt_secret, {
      expiresIn: config.jwt_max_age,
      algorithm: 'HS256'
  })

  return token

};

function verifyJwtToken(token) {

  return new Promise((resolve, reject) => {
    jwt.verify(token, config.jwt_secret, (err, decodedToken) => {
      if (err || !decodedToken) {
        return reject(err)
      } resolve(decodedToken)
    })
  })

}

module.exports = {
  saltHashPassword,
  randomToken,
  createJwtToken,
  verifyJwtToken
}
