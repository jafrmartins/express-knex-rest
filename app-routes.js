/**
 * Routes
 * Note: the order of the routes is relevant. Most Generic routes at the bottom.
 */
module.exports = [
  
  /* auth */

  { "method": "post", "url": "/auth/login", "callback": "./middleware/auth#login" },
  { "method": "post", "url": "/auth/register", "callback": "./middleware/auth#register" },
  { "method": "post", "url": "/auth/refresh-token", "callback": "./middleware/auth#refresh_token" },
  
  /* knex rest */

  { "method": "get", "url": "/rest/:connection/:table/:q", "callback": "./middleware/rest#get", "allow": ["admin"] },
  { "method": "put", "url": "/rest/:connection/:table/:q", "callback": "./middleware/rest#put", "allow": ["admin"] },
  { "method": "delete", "url": "/rest/:connection/:table/:q", "callback": "./middleware/rest#delete", "allow": ["admin"] },
  { "method": "post", "url": "/rest/:connection/:table", "callback": "./middleware/rest#post", "allow": ["admin"] },
  { "method": "get", "url": "/rest/:connection/:table", "callback": "./middleware/rest#all", "allow": ["admin"] },
  
]