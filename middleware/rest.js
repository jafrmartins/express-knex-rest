const rest = require('../models/rest')

function getConnectionName(req) {
  if (req.params.connection) {
    return req.params.connection
  }
  const parts = req.originalUrl.replace(/^\//, '').split("/");
  return parts[1].split("-").join("_")
}

function getTableName(req) {
  if (req.params.table) {
    return req.params.table
  }
  const parts = req.originalUrl.replace(/^\//, '').split("/");
  return parts[2].split("-").join("_")
}

function getQuery(param) {
  param = JSON.parse(param);
  if (typeof param == "number") {
    return { id: param }
  } return param
}

function getLimit(req) {
  let limit = req.query.limit
  if(limit) {
    limit = Number(limit)
  } return limit
}

function formatError(err) {
  return { message: err.message, stack: err.stack }
}

module.exports = {
  "all": (req, res) => {
    const connection = getConnectionName(req)
    const table = getTableName(req)
    const limit = getLimit(req)
    rest.all({ table, connection, limit }).then((data) => {
      req.events.emit(`rest:${connection}:${table}:all`, data)
      res.status(200).json(data)
    }).catch((err) => {
      res.status(500).json(formatError(err))
    })
  },
  "get": (req, res) => {
    const connection = getConnectionName(req)
    const table = getTableName(req)
    const q = getQuery(req.params.q)
    const limit = getLimit(req)
    rest.get({ table, q, connection, limit }).then((data) => {
      req.events.emit(`rest:${connection}:${table}:get`, data)
      res.status(200).json(data)
    }).catch((err) => {
      res.status(500).json(formatError(err))
    })
  },
  "post": (req, res) => {
    const connection = getConnectionName(req)
    const table = getTableName(req)
    const body = req.body
    rest.post({ table, body, connection }).then((id) => {
      rest.get({ table, connection, q: { id } }).then((data) => {
        req.events.emit(`rest:${connection}:${table}:post`, data)
        res.status(200).json({id})
      }).catch((err) => {
        res.status(500).json(formatError(err))
      })
    }).catch((err) => {
      res.status(500).json(formatError(err))
    })
  },
  "put": (req, res) => {
    const connection = getConnectionName(req)
    const table = getTableName(req)
    const q = getQuery(req.params.q)
    const body = req.body
    rest.put({ table, body, q, connection }).then(() => {
      rest.get({ table, connection, q }).then((data) => {
        req.events.emit(`rest:${connection}:${table}:put`, data)
        res.status(200).json(data)
      }).catch((err) => {
        res.status(500).json(formatError(err))
      })
    }).catch((err) => {
      res.status(500).json(formatError(err))
    })
  },
  "delete": (req, res) => {
    const connection = getConnectionName(req)
    const table = getTableName(req)
    const q = getQuery(req.params.q)
    rest.get({ table, connection, q }).then((data) => {
      req.events.emit(`rest:${connection}:${table}:delete`, data)
      rest.delete({ table, q, connection }).then(() => {
        res.status(200).json(q)
      }).catch((err) => {
        res.status(500).json(formatError(err))
      })
    }).catch((ecvbnrr) => {
      res.status(500).json(formatError(err))
    })
  }
}