const { Router } = require('express')
const { bearer_jwt_token, allow } = require('./auth')
const { resolve } = require('path')

const routes = require("../app-routes")

module.exports = () => {
    const app = new Router()
    for (let i = 0; i < routes.length; i++) {
        const route = routes[i];
        const r = new Router
        if (route.allow) {
            r.use(route.url, bearer_jwt_token)
            r.use(route.url, allow({ grants: route.allow }))
        }
        let parts = route.callback.split("#")
        let callback = require(resolve(process.cwd(), parts.shift()))
        if (exp = parts.shift()) { callback = callback[exp] }
        r[route.method](route.url, callback)
        app.use("/", r)
    }
    return app
}