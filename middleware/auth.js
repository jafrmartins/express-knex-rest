const config = require('../expressfile')
const auth = require('../models/auth')
const { verifyJwtToken, saltHashPassword, createJwtToken } = require('../lib/utils')

const unauthorized = { message: 'Format is Authorization: Bearer [token]' }
const badRequest = { message: 'Invalid Token' }
const unAllowed = { message: 'You are not allowed access to this resource' }
const unauthorizedEmail = { message: 'Invalid Email' }

module.exports = {
  login(req, res) {
    const { email, password } = req.body
    auth.getUserByEmail({ email }).then((user) => {
      const { hash } = saltHashPassword({
        password,
        salt: user.salt
      })
      if(hash === user.password) {
        let token = createJwtToken(user);
        auth.createUserToken(user.id)
        .then((refresh_token) => {
          res.status(200).json({ token, refresh_token, expires_in: config.jwt_max_age })
        })
      }
    }).catch((error) => {
      res.status(500).json({ error: error.message })
    })
  },
  refresh_token(req, res) {
    const { email, refresh_token } = req.body
    auth.verifyUserToken(refresh_token)
    .then((user) => {
      if (user.email != email ) {
        return res.status(401).json(unauthorizedEmail)
      } else {
        auth.deleteUserToken(refresh_token)
        .then(() => {
          let token = createJwtToken(user);
          auth.createUserToken(user.id)
          .then((refresh_token) => {
            res.status(200).json({ token, refresh_token, expires_in: config.jwt_max_age })
          })
        })
      }
    }).catch((error) => {
      res.status(500).json({ error: error.message })
    })
  },
  register(req, res) {
    const { email, password } = req.body
    auth.registerUser({ email, password }).then((id) => {
      res.status(200).json({ message: 'success', id })
    }).catch((error) => {
      res.status(500).json({ error: error.message })
    })  
  },
  bearer_jwt_token(req, res, next) {
    if (req.headers.authorization) {
      let parts = req.headers.authorization.split(' ');
      if (parts.length == 2) {
        let scheme = parts[0];
        let token = parts[1];
        if (/^Bearer$/i.test(scheme)) {
          verifyJwtToken(token).then((decoded) => {
            req.user = decoded.data
            if (req.user.id) {
              next()
            } else  {
              res.status(400).json(badRequest)
            }
          }).catch((err) => {
            res.status(400).json(badRequest)
          })
        }
      } else {
        res.status(401).json(unauthorized)
      }
    } else {
      res.status(401).json(unauthorized)
    }
  },
  allow({grants}) {
    return (req, res, next) => {
      if (!req.user.grants) {
        res.status(401).json(unauthorized)
      }
      let isAllowed = false
      for (let i = 0; i < grants.length; i++) {
        const grant = grants[i]
        if (/^\$/.test(grant)) {

          switch (grant) {
            case "$me":
              isAllowed = $me(req)
              break;
          
            default:
              break;
          }
          
          if (isAllowed) { break }

        } else if (req.user.grants.indexOf(grant) != -1) {
          isAllowed = true
          break
        }
      }
      if (isAllowed) { return next() }
      res.status(401).json(unAllowed)
    }
  }
}

function $me(req) {
  console.log(req.path);
  if(req.params.id) {
    return req.params.id == req.user.id  
  }
  return Number(req.originalUrl.split("/").pop()) == req.user.id
}