
exports.seed = (knex, Promise) => {
  return Promise.all([
    knex('grants').del().then(() => {
      return knex('grants').insert([
        { name: 'admin'}
      ])
    })
  ])
};
